FROM python:3.6-alpine
ADD . /code
WORKDIR /code
RUN pip install pipenv
RUN pipenv --python 3.6
RUN pipenv install --system

